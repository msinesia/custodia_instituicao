package com.itau.custodiaInstituicao.service;

import com.itau.custodiaInstituicao.dto.ContratoInstituicaoDTO;
import com.itau.custodiaInstituicao.repository.CustodiaInstituicaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CustodiaInstituicaoService {

    @Autowired
    private CustodiaInstituicaoRepository custodiaInstituicaoRepository;

    //valida token da instituicao na base de dados
    public boolean validaToken(String token) {
    }

    //valida cliente existe e se autoriza consulta externa
    public boolean validaCliente(String cpf) {
    }

    //retorna os dados do contrato do cliente
    public ContratoInstituicaoDTO getContrato(String cpf) {
    }

}
