package com.itau.custodiaInstituicao.controller;
import com.itau.custodiaInstituicao.service.CustodiaInstituicaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//
@RestController
@RequestMapping ("/instituicao")
public class InstituicaoController {

    @Autowired
    private CustodiaInstituicaoService custodiaInstituicaoService;

    @RequestMapping("/{cpf},{token}")
    public void consultarContratos(String cpf, String token){

        if (custodiaInstituicaoService.validaToken(token)){

            if (custodiaInstituicaoService.validaCliente(cpf)){

                custodiaInstituicaoService.getContrato(cpf);

            }//cliente não encontrado ou consulta não autorizada

        } //instituicao não autorizada



    }

}
