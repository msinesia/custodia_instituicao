package com.itau.custodiaInstituicao.dto;

import java.util.Date;

public class ContratoInstituicaoDTO {
    protected String status_contrato;
    protected double valor_total_contrato;
    protected double valor_aberto_contrato;
    protected Date dt_fim_contrato;

    public ContratoInstituicaoDTO(String status_contrato, double valor_total_contrato, double valor_aberto_contrato, Date dt_fim_contrato) {
        this.status_contrato = status_contrato;
        this.valor_total_contrato = valor_total_contrato;
        this.valor_aberto_contrato = valor_aberto_contrato;
        this.dt_fim_contrato = dt_fim_contrato;
    }

    public String getStatus_contrato() {
        return status_contrato;
    }

    public void setStatus_contrato(String status_contrato) {
        this.status_contrato = status_contrato;
    }

    public double getValor_total_contrato() {
        return valor_total_contrato;
    }

    public void setValor_total_contrato(double valor_total_contrato) {
        this.valor_total_contrato = valor_total_contrato;
    }

    public double getValor_aberto_contrato() {
        return valor_aberto_contrato;
    }

    public void setValor_aberto_contrato(double valor_aberto_contrato) {
        this.valor_aberto_contrato = valor_aberto_contrato;
    }

    public Date getDt_fim_contrato() {
        return dt_fim_contrato;
    }

    public void setDt_fim_contrato(Date dt_fim_contrato) {
        this.dt_fim_contrato = dt_fim_contrato;
    }
}
